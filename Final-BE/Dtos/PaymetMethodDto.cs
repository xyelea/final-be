﻿namespace Final_BE.Dtos
{
    public class CreatePaymentMethodDto
    {
        public string Name { get; set; }
        public string Image { get; set; }
        public bool IsActive { get; set; }
    }
}
