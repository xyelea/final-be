﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Final_BE.Dtos;
using Final_BE.Models;
using Microsoft.Extensions.Configuration;

namespace Final_BE.Repositories
{
    public class PaymentMethodRepository : IPaymentMethodRepository
    {
        private readonly string _connectionString;

        public PaymentMethodRepository(IConfiguration configuration)
        {
            _connectionString = configuration.GetConnectionString("DefaultConnection");
        }

        public List<PaymentMethod> GetAll()
        {
            List<PaymentMethod> paymentMethods = new List<PaymentMethod>();

            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                string query = "SELECT * FROM PaymentMethod";
                SqlCommand command = new SqlCommand(query, connection);

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        PaymentMethod paymentMethod = new PaymentMethod
                        {
                            IdPaymentMethod = reader.GetGuid(reader.GetOrdinal("IdPaymentMethod")),
                            Name = reader.GetString(reader.GetOrdinal("Name")),
                            Image = reader.GetString(reader.GetOrdinal("Image")),
                            IsActive = reader.GetBoolean(reader.GetOrdinal("IsActive"))
                        };

                        paymentMethods.Add(paymentMethod);
                    }
                }
            }

            return paymentMethods;
        }

        public PaymentMethod GetById(Guid id)
        {
            PaymentMethod paymentMethod = null;

            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                string query = "SELECT * FROM PaymentMethod WHERE IdPaymentMethod = @IdPaymentMethod";
                SqlCommand command = new SqlCommand(query, connection);
                command.Parameters.AddWithValue("@IdPaymentMethod", id);

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        paymentMethod = new PaymentMethod
                        {
                            IdPaymentMethod = reader.GetGuid(reader.GetOrdinal("IdPaymentMethod")),
                            Name = reader.GetString(reader.GetOrdinal("Name")),
                            Image = reader.GetString(reader.GetOrdinal("Image")),
                            IsActive = reader.GetBoolean(reader.GetOrdinal("IsActive"))
                        };
                    }
                }
            }

            return paymentMethod;
        }

        public Guid AddPaymentMethod(CreatePaymentMethodDto paymentMethodDto)
        {
            Guid id = Guid.NewGuid();

            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                string query = "INSERT INTO PaymentMethod (IdPaymentMethod, Name, Image, IsActive) VALUES (@IdPaymentMethod, @Name, @Image, @IsActive)";
                SqlCommand command = new SqlCommand(query, connection);
                command.Parameters.AddWithValue("@IdPaymentMethod", id);
                command.Parameters.AddWithValue("@Name", paymentMethodDto.Name);
                command.Parameters.AddWithValue("@Image", paymentMethodDto.Image);
                command.Parameters.AddWithValue("@IsActive", paymentMethodDto.IsActive);

                command.ExecuteNonQuery();
            }

            return id;
        }

        public void Update(PaymentMethod paymentMethod)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                string query = "UPDATE PaymentMethod SET Name = @Name, Image = @Image, IsActive = @IsActive WHERE IdPaymentMethod = @IdPaymentMethod";
                SqlCommand command = new SqlCommand(query, connection);
                command.Parameters.AddWithValue("@Name", paymentMethod.Name);
                command.Parameters.AddWithValue("@Image", paymentMethod.Image);
                command.Parameters.AddWithValue("@IsActive", paymentMethod.IsActive);
                command.Parameters.AddWithValue("@IdPaymentMethod", paymentMethod.IdPaymentMethod);

                command.ExecuteNonQuery();
            }
        }

        public void Delete(Guid id)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                string query = "DELETE FROM PaymentMethod WHERE IdPaymentMethod = @IdPaymentMethod";
                SqlCommand command = new SqlCommand(query, connection);
                command.Parameters.AddWithValue("@IdPaymentMethod", id);

                command.ExecuteNonQuery();
            }
        }


    }
}
