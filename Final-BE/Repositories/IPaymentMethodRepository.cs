﻿using System;
using System.Collections.Generic;
using Final_BE.Dtos;
using Final_BE.Models;

namespace Final_BE.Repositories
{
    public interface IPaymentMethodRepository
    {
        List<PaymentMethod> GetAll();
        PaymentMethod GetById(Guid id);
        Guid AddPaymentMethod(CreatePaymentMethodDto paymentMethodDto);
        void Update(PaymentMethod paymentMethod);
        void Delete(Guid id);
    }
}
