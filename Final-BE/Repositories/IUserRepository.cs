﻿using System;
using System.Collections.Generic;
using Final_BE.Models;

namespace Final_BE.Repositories
{
    public interface IUserRepository
    {
        List<User> GetAll();
        User GetById(Guid id);
        void Add(User user);
        void Update(User user);
        void Delete(Guid id);
    }
}
