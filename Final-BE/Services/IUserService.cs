﻿using System;
using System.Collections.Generic;
using Final_BE.Dtos;
using Final_BE.Models;

namespace Final_BE.Services
{
    public interface IUserService
    {
        List<User> GetAllUsers();
        User GetUserById(Guid id);
        void AddUser(UserDto userDto);
        void DeleteUser(Guid id);
        void UpdateUser(User user);
    }
}
