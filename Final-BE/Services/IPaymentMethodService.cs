﻿using System;
using System.Collections.Generic;
using Final_BE.Dtos;
using Final_BE.Models;

namespace Final_BE.Services
{
    public interface IPaymentMethodService
    {
        List<PaymentMethod> GetAllPaymentMethods();
        PaymentMethod GetPaymentMethodById(Guid id);
        Guid AddPaymentMethod(CreatePaymentMethodDto paymentMethodDto);
        void UpdatePaymentMethod(PaymentMethod paymentMethod);
        void DeletePaymentMethod(Guid id);
    }
}
