﻿using System;
using System.Collections.Generic;
using Final_BE.Dtos;
using Final_BE.Models;
using Final_BE.Repositories;

namespace Final_BE.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;

        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public List<User> GetAllUsers()
        {
            return _userRepository.GetAll();
        }

        public User GetUserById(Guid id)
        {
            return _userRepository.GetById(id);
        }

        public void AddUser(UserDto userDto)
        {
            if (IsNameExists(userDto.Name))
            {
                throw new Exception("Nama sudah digunakan oleh pengguna lain.");
            }

            if (IsEmailExists(userDto.Email))
            {
                throw new Exception("Email sudah digunakan oleh pengguna lain.");
            }

            User user = new User
            {
                Id = Guid.NewGuid(),
                Name = userDto.Name,
                Email = userDto.Email,
                Password = userDto.Password,
                Role = false,
                IsActive = false
            };

            _userRepository.Add(user);
        }
        private bool IsNameExists(string name)
        {
            List<User> users = _userRepository.GetAll();
            return users.Any(u => u.Name == name);
        }

        private bool IsEmailExists(string email)
        {
            List<User> users = _userRepository.GetAll();
            return users.Any(u => u.Email == email);
        }

        public void UpdateUser(User user)
        {
            User _user = new User
            {
            
                Name = user.Name,
                Email = user.Email,
                Password = user.Password,
                Role = false,
                IsActive = false
            };

            _userRepository.Update(user);
        }

        public void DeleteUser(Guid id)
        {
            _userRepository.Delete(id);
        }

        
    }
}
