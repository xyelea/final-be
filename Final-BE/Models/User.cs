﻿using System;

namespace Final_BE.Models
{
    public class User
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public bool Role { get; set; }
        public bool IsActive { get; set; }
    }
}
