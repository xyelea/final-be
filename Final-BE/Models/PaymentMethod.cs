﻿using System;

namespace Final_BE.Models
{
    public class PaymentMethod
    {
        public Guid IdPaymentMethod { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }
        public bool IsActive { get; set; }
    }
}
