﻿using otomobil.Dtos;
using otomobil.Models;
using otomobil.Repositories;
using Microsoft.Extensions.Configuration;
using System.Data.SqlClient;

namespace otomobil.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly IConfiguration _configuration;
        private readonly string ConnectionString;

        public UserRepository(IConfiguration configuration)
        {
            _configuration = configuration;
            this.ConnectionString = _configuration.GetConnectionString("DefaultConnection");
        }

        public bool CreateUserAccount(User user, UserRole userRole)
        {
            bool result = false;

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                con.Open();

                SqlTransaction tran = con.BeginTransaction();

                try
                {
                    // First query
                    SqlCommand cmdUser = new SqlCommand();
                    cmdUser.Connection = con;
                    cmdUser.Transaction = tran;
                    cmdUser.Parameters.Clear();

                    cmdUser.CommandText = "INSERT INTO Users VALUES (@Id, @Username, @Password, @Email, @IsActivated)";
                    cmdUser.Parameters.AddWithValue("@Id", user.Id);
                    cmdUser.Parameters.AddWithValue("@Username", user.Username);
                    cmdUser.Parameters.AddWithValue("@Password", user.Password);
                    cmdUser.Parameters.AddWithValue("@Email", user.Email);
                    cmdUser.Parameters.AddWithValue("@IsActivated", user.IsActivated);

                    // Second Query
                    SqlCommand cmdRole = new SqlCommand();
                    cmdRole.Connection = con;
                    cmdRole.Transaction = tran;
                    cmdRole.Parameters.Clear();

                    cmdRole.CommandText = "INSERT INTO UserRoles VALUES (@UserId, @Role)";
                    cmdRole.Parameters.AddWithValue("@UserId", userRole.UserId);
                    cmdRole.Parameters.AddWithValue("@Role", userRole.Role);

                    var resultUser = cmdUser.ExecuteNonQuery();
                    var resultRole = cmdRole.ExecuteNonQuery();

                    tran.Commit();

                    result = true;
                }
                catch (Exception ex)
                {
                    tran.Rollback();
                    throw;
                }
                finally
                {
                    con.Close();
                }
            }

            return result;
        }
        public bool IsUsernameExists(string username)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                con.Open();

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.Parameters.Clear();

                cmd.CommandText = "SELECT COUNT(*) FROM Users WHERE Username = @Username";
                cmd.Parameters.AddWithValue("@Username", username);

                int count = (int)cmd.ExecuteScalar();

                con.Close();
                return count > 0;
            }
        }

        public bool IsEmailExists(string email)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                con.Open();

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.Parameters.Clear();

                cmd.CommandText = "SELECT COUNT(*) FROM Users WHERE Email = @Email";
                cmd.Parameters.AddWithValue("@Email", email);

                int count = (int)cmd.ExecuteScalar();

                con.Close();
                return count > 0;
            }
        }

        public UserGetDto GetUserById(Guid id)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                con.Open();

                UserGetDto userDto = null;

                string userQuery = "SELECT Id, Username, Email, Password, IsActivated FROM Users WHERE Id = @Id";

                using (SqlCommand cmdUser = new SqlCommand(userQuery, con))
                {
                    cmdUser.Parameters.AddWithValue("@Id", id);

                    using (SqlDataReader userReader = cmdUser.ExecuteReader())
                    {
                        if (userReader.Read())
                        {
                            userDto = new UserGetDto
                            {
                                Id = (Guid)userReader["Id"],
                                Username = (string)userReader["Username"],
                                Email = (string)userReader["Email"],
                                Password = (string)userReader["Password"],
                                IsActive = (bool)userReader["IsActivated"]
                            };
                        }
                    }
                }

                if (userDto != null)
                {
                    string roleQuery = "SELECT Role FROM UserRoles WHERE UserId = @UserId";

                    using (SqlCommand cmdRole = new SqlCommand(roleQuery, con))
                    {
                        cmdRole.Parameters.AddWithValue("@UserId", id);

                        using (SqlDataReader roleReader = cmdRole.ExecuteReader())
                        {
                            if (roleReader.Read())
                            {
                                userDto.Role = (string)roleReader["Role"];
                            }
                        }
                    }
                }

                return userDto;
            }
        }

        public List<UserGetDto> GetAllUsers()
        {
            List<UserGetDto> users = new List<UserGetDto>();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                con.Open();

                SqlTransaction tran = con.BeginTransaction();

                try
                {
                    SqlCommand cmdUsers = new SqlCommand();
                    cmdUsers.Connection = con;
                    cmdUsers.Transaction = tran;
                    cmdUsers.Parameters.Clear();

                    cmdUsers.CommandText = "SELECT Id, Username, Email, Password, IsActivated FROM Users";

                    using (SqlDataReader userReader = cmdUsers.ExecuteReader())
                    {
                        while (userReader.Read())
                        {
                            UserGetDto user = new UserGetDto
                            {
                                Id = userReader.GetGuid(0),
                                Username = userReader.GetString(1),
                                Email = userReader.GetString(2),
                                Password = userReader.GetString(3),
                                IsActive = userReader.GetBoolean(4)
                            };

                            users.Add(user);
                        }
                    }

                    SqlCommand cmdUserRoles = new SqlCommand();
                    cmdUserRoles.Connection = con;
                    cmdUserRoles.Transaction = tran;
                    cmdUserRoles.Parameters.Clear();

                    cmdUserRoles.CommandText = "SELECT UserId, Role FROM UserRoles";

                    using (SqlDataReader userRoleReader = cmdUserRoles.ExecuteReader())
                    {
                        while (userRoleReader.Read())
                        {
                            Guid userId = userRoleReader.GetGuid(0);
                            string role = userRoleReader.GetString(1);

                            UserGetDto user = users.FirstOrDefault(u => u.Id == userId);
                            if (user != null)
                            {
                                user.Role = role;
                            }
                        }
                    }

                    tran.Commit();
                }
                catch (Exception ex)
                {
                    tran.Rollback();
                    throw;
                }
                finally
                {
                    con.Close();
                }
            }

            return users;
        }

        public List<UserGetDto> GetAllUsers(int limit)
        {
            List<UserGetDto> users = new List<UserGetDto>();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                con.Open();

                SqlTransaction tran = con.BeginTransaction();

                try
                {
                    SqlCommand cmdUsers = new SqlCommand();
                    cmdUsers.Connection = con;
                    cmdUsers.Transaction = tran;
                    cmdUsers.Parameters.Clear();

                    cmdUsers.CommandText = "SELECT TOP (@Limit) Id, Username, Email, Password, IsActivated FROM Users";
                    cmdUsers.Parameters.AddWithValue("@Limit", limit);

                    using (SqlDataReader userReader = cmdUsers.ExecuteReader())
                    {
                        while (userReader.Read())
                        {
                            UserGetDto user = new UserGetDto
                            {
                                Id = userReader.GetGuid(0),
                                Username = userReader.GetString(1),
                                Email = userReader.GetString(2),
                                Password = userReader.GetString(3),
                                IsActive = userReader.GetBoolean(4)
                            };

                            users.Add(user);
                        }
                    }

                    SqlCommand cmdUserRoles = new SqlCommand();
                    cmdUserRoles.Connection = con;
                    cmdUserRoles.Transaction = tran;
                    cmdUserRoles.Parameters.Clear();

                    cmdUserRoles.CommandText = "SELECT UserId, Role FROM UserRoles";

                    using (SqlDataReader userRoleReader = cmdUserRoles.ExecuteReader())
                    {
                        while (userRoleReader.Read())
                        {
                            Guid userId = userRoleReader.GetGuid(0);
                            string role = userRoleReader.GetString(1);

                            UserGetDto user = users.FirstOrDefault(u => u.Id == userId);
                            if (user != null)
                            {
                                user.Role = role;
                            }
                        }
                    }

                    tran.Commit();
                }
                catch (Exception ex)
                {
                    tran.Rollback();
                    throw;
                }
                finally
                {
                    con.Close();
                }
            }

            return users;
        }

        public bool UpdateUser(Guid id, UserUpdateDto userDto)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                con.Open();

                SqlTransaction tran = con.BeginTransaction();

                try
                {
                    SqlCommand cmdUser = new SqlCommand();
                    cmdUser.Connection = con;
                    cmdUser.Transaction = tran;
                    cmdUser.Parameters.Clear();

                    cmdUser.CommandText = "UPDATE Users SET Username = @Username, Password = @Password, Email = @Email, IsActivated = @IsActivated WHERE Id = @Id";
                    cmdUser.Parameters.AddWithValue("@Id", id);
                    cmdUser.Parameters.AddWithValue("@Username", userDto.Username);
                    cmdUser.Parameters.AddWithValue("@Password", userDto.Password);
                    cmdUser.Parameters.AddWithValue("@Email", userDto.Email);
                    cmdUser.Parameters.AddWithValue("@IsActivated", userDto.IsActive);

                    int userResult = cmdUser.ExecuteNonQuery();

                    SqlCommand cmdUserRole = new SqlCommand();
                    cmdUserRole.Connection = con;
                    cmdUserRole.Transaction = tran;
                    cmdUserRole.Parameters.Clear();

                    cmdUserRole.CommandText = "UPDATE UserRoles SET Role = @Role WHERE UserId = @UserId";
                    cmdUserRole.Parameters.AddWithValue("@UserId", id);
                    cmdUserRole.Parameters.AddWithValue("@Role", userDto.Role);

                    int userRoleResult = cmdUserRole.ExecuteNonQuery();

                    tran.Commit();

                    return userResult > 0 && userRoleResult > 0;
                }
                catch (Exception ex)
                {
                    tran.Rollback();
                    throw;
                }
                finally
                {
                    con.Close();
                }
            }
        }


        public bool DeleteUser(Guid id)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                con.Open();

                SqlTransaction tran = con.BeginTransaction();

                try
                {
                    // Delete User
                    SqlCommand cmdUser = new SqlCommand();
                    cmdUser.Connection = con;
                    cmdUser.Transaction = tran;
                    cmdUser.Parameters.Clear();

                    cmdUser.CommandText = "DELETE FROM Users WHERE Id = @Id";
                    cmdUser.Parameters.AddWithValue("@Id", id);

                    int userResult = cmdUser.ExecuteNonQuery();

                    // Delete UserRole
                    SqlCommand cmdUserRole = new SqlCommand();
                    cmdUserRole.Connection = con;
                    cmdUserRole.Transaction = tran;
                    cmdUserRole.Parameters.Clear();

                    cmdUserRole.CommandText = "DELETE FROM UserRoles WHERE UserId = @UserId";
                    cmdUserRole.Parameters.AddWithValue("@UserId", id);

                    int userRoleResult = cmdUserRole.ExecuteNonQuery();

                    tran.Commit();

                    return userResult > 0 && userRoleResult > 0;
                }
                catch (Exception ex)
                {
                    tran.Rollback();
                    throw;
                }
                finally
                {
                    con.Close();
                }
            }
        }
    }
}
