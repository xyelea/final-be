﻿using System.Data.SqlClient;
using otomobil.Models;

namespace otomobil.Repositories
{
    public class CourseRepository
    {
        private readonly IConfiguration _configuration;
        private readonly string ConnectionString;

        public CourseRepository(IConfiguration configuration)
        {
            _configuration = configuration;
            this.ConnectionString = configuration.GetConnectionString("DefaultConnection");
        }

        public List<Course> GetCourses()
        {
            List<Course> course = new List<Course>();

            string query = "SELECT co.id_course,co.course_name,co.course_description,co.course_price,co.course_image,ca.name FROM Course co join Category ca ON ca.id_category = co.id_category ";

            using(SqlConnection connection = new SqlConnection(ConnectionString))
            {
                using(SqlCommand cmd = new SqlCommand(query,connection))
                {
                    try
                    {
                        connection.Open();
                        using(SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                course.Add(new Course()
                                {
                                    Id= Convert.ToInt32(reader["id_course"]),
                                    Name= reader["course_name"].ToString()??string.Empty,
                                    Description = reader["course_description"].ToString()??string.Empty,
                                    Price = Convert.ToInt32(reader["course_price"]),
                                    Image = reader["course_image"].ToString()??string.Empty,
                                    Category = new Category
                                    {
                                        Name = reader["name"].ToString()??string.Empty,
                                    }
                                });
                            }
                        }
                    }catch (Exception ex)
                    {
                        throw;
                    }finally 
                    { 
                        connection.Close();
                    }
                }
            }

            return course;
        }

        public List<Course> GetCoursesOther(int id)
        {
            List<Course> course = new List<Course>();

            string query = "SELECT co.id_course,co.course_name,co.course_description,co.course_price,co.course_image,ca.name FROM Course co join Category ca ON ca.id_category = co.id_category where co.id_course != @id ";

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    try
                    {
                        cmd.Connection = connection;
                        cmd.Parameters.Clear();
                        cmd.CommandText = query;
                        cmd.Parameters.AddWithValue("@id", id);

                        connection.Open();
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                course.Add(new Course()
                                {
                                    Id = Convert.ToInt32(reader["id_course"]),
                                    Name = reader["course_name"].ToString() ?? string.Empty,
                                    Description = reader["course_description"].ToString() ?? string.Empty,
                                    Price = Convert.ToInt32(reader["course_price"]),
                                    Image = reader["course_image"].ToString() ?? string.Empty,
                                    Category = new Category
                                    {
                                        Name = reader["name"].ToString() ?? string.Empty,
                                    }
                                });
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            return course;
        }

        public List<Course> GetCoursesByCategory(string name)
        {
            List<Course> course = new List<Course>();

            string query = "SELECT co.id_course,co.course_name,co.course_description,co.course_price,co.course_image,ca.name FROM Course co join Category ca ON ca.id_category = co.id_category where ca.name = @name";

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    try
                    {
                        cmd.Connection = connection;
                        cmd.Parameters.Clear();
                        cmd.CommandText = query;
                        cmd.Parameters.AddWithValue("@name", name);

                        connection.Open();
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                course.Add(new Course()
                                {
                                    Id = Convert.ToInt32(reader["id_course"]),
                                    Name = reader["course_name"].ToString() ?? string.Empty,
                                    Description = reader["course_description"].ToString() ?? string.Empty,
                                    Price = Convert.ToInt32(reader["course_price"]),
                                    Image = reader["course_image"].ToString() ?? string.Empty,
                                    Category = new Category
                                    {
                                        Name = reader["name"].ToString() ?? string.Empty,
                                    }
                                });
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            return course;
        }

        public Course? GetById(int id)
        {
            Course? course = null;

            string query = "SELECT co.id_course,co.course_name,co.course_description,co.course_price,co.course_image,ca.name FROM Course co join Category ca ON ca.id_category = co.id_category WHERE co.id_course = @id";

            using(SqlConnection connection = new SqlConnection(ConnectionString))
            {
                using(SqlCommand cmd = new SqlCommand())
                {
                    try
                    {
                        cmd.Connection = connection;
                        cmd.Parameters.Clear();
                        cmd.CommandText = query;
                        cmd.Parameters.AddWithValue("@id", id);

                        connection.Open();
                        using(SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                course = new Course()
                                {
                                    Id = Convert.ToInt32(reader["id_course"]),
                                    Name = reader["course_name"].ToString() ?? string.Empty,
                                    Description = reader["course_description"].ToString() ?? string.Empty,
                                    Price = Convert.ToInt32(reader["course_price"]),
                                    Image = reader["course_image"].ToString() ?? string.Empty,
                                    Category = new Category
                                    {
                                        Name = reader["name"].ToString() ?? string.Empty,
                                    }
                                };
                            }
                        }

                    }catch (Exception ex)
                    {

                    }finally { connection.Close(); }
                }
            }

            return course;
        }
    }
}
