﻿using System;
using System.Collections.Generic;
using otomobil.Dtos;
using otomobil.Models;

namespace otomobil.Repositories
{
    public interface IUserRepository
    {
        bool CreateUserAccount(User user, UserRole userRole);
        bool IsUsernameExists(string username);
        bool IsEmailExists(string email);
        UserGetDto GetUserById(Guid id);
        List<UserGetDto> GetAllUsers();
        List<UserGetDto> GetAllUsers(int limit);
        bool UpdateUser(Guid id, UserUpdateDto userDto);
        bool DeleteUser(Guid id);
    }
}
