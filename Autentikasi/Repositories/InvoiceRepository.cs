﻿using System.Data.SqlClient;
using otomobil.Dtos;
using otomobil.Models;

namespace otomobil.Repositories
{
    public class InvoiceRepository
    {
        private readonly IConfiguration _configuration;
        private readonly string ConnectionString;

        public InvoiceRepository(IConfiguration configuration)
        {
            _configuration = configuration;
            this.ConnectionString = configuration.GetConnectionString("DefaultConnection");
        }

        public bool CreateInvoice(Invoice invoice, List<int> cartIds)
        {
            bool result = false;

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();

                try
                {
                    SqlCommand cmdInvoice = new SqlCommand();
                    cmdInvoice.Connection = connection;
                    cmdInvoice.Transaction = transaction;
                    cmdInvoice.Parameters.Clear();

                    cmdInvoice.CommandText = "INSERT INTO invoice VALUES (@IdInvoice,@Iduser,@IdPayment,@tanggal)";
                    cmdInvoice.Parameters.AddWithValue("@IdInvoice", invoice.Id);
                    cmdInvoice.Parameters.AddWithValue("@Iduser", invoice.UserId);
                    cmdInvoice.Parameters.AddWithValue("@IdPayment", invoice.PaymentId);
                    cmdInvoice.Parameters.AddWithValue("@tanggal", invoice.Date);
                    cmdInvoice.ExecuteNonQuery();

                    SqlCommand cmdUpdate = new SqlCommand();
                    cmdUpdate.Connection = connection;
                    cmdUpdate.Transaction = transaction;
                    cmdUpdate.Parameters.Clear();

                    cmdUpdate.CommandText = "UPDATE cart_course SET id_invoice = @IdInvoice WHERE id_user = @IdUser AND id_cart_course IN (";
                    for (int i = 0; i < cartIds.Count; i++)
                    {
                        string paramName = "@IdCart" + i;
                        cmdUpdate.CommandText += paramName + ",";
                        cmdUpdate.Parameters.AddWithValue(paramName, cartIds[i]);
                    }
                    cmdUpdate.CommandText = cmdUpdate.CommandText.TrimEnd(',') + ") AND id_invoice = ''";
                    cmdUpdate.Parameters.AddWithValue("@IdInvoice", invoice.Id);
                    cmdUpdate.Parameters.AddWithValue("@Iduser", invoice.UserId);
                    cmdUpdate.ExecuteNonQuery();

                    transaction.Commit();
                    result = true;

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw;
                }
                finally
                {
                    connection.Close();
                }

                
            }
            return result;
        }

        public bool CheckIfCartExist(Guid userId)
        {
            string query = "SELECT COUNT(*) FROM cart_course WHERE id_user = @UserId AND id_invoice = ''";
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = con;
                    cmd.Parameters.Clear();
                    cmd.CommandText = query;
                    cmd.Parameters.AddWithValue("@UserId", userId);

                    try
                    {
                        con.Open();
                        int count = (int)cmd.ExecuteScalar();
                        return count > 0;
                    }
                    catch (Exception ex)
                    {
                        throw;
                    }
                    finally { con.Close(); }

                }
            }
        }

        public List<InvoiceDisplayDto> GetAllInvoice(Guid UserId)
        {
            List<InvoiceDisplayDto> invoices = new List<InvoiceDisplayDto>();

            string query = "SELECT c.id_invoice,i.tanggal, COUNT(*) as total_course,SUM(c.course_price) as total_price " +
                "FROM cart_course c join invoice i ON c.id_invoice = i.id_invoice WHERE i.id_user = @UserId group by c.id_invoice,i.tanggal ORDER BY i.tanggal DESC";
            using(SqlConnection con = new SqlConnection(ConnectionString))
            {
                using(SqlCommand cmd = new SqlCommand(query,con))
                {
                    cmd.Parameters.Clear();
                    cmd.Parameters.AddWithValue("@UserId", UserId);
                    try
                    {
                        con.Open();
                        using(SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                InvoiceDisplayDto invoice = new InvoiceDisplayDto()
                                {
                                    Id = reader["id_invoice"].ToString() ?? string.Empty,
                                    Date = Convert.ToDateTime(reader["tanggal"]),
                                    TotalCourse = Convert.ToInt32(reader["total_course"]),
                                    TotalPrice = Convert.ToInt32(reader["total_price"])
                                };
                                invoices.Add(invoice);
                            }
                        }

                    }catch (Exception ex)
                    {
                        throw;
                    }finally { con.Close(); }
                }
            }

            return invoices;
        }

        public InvoiceDetailDto? GetDetailInvoice(string InvoiceId)
        {
            InvoiceDetailDto? invoice = null;

            string query = "SELECT c.id_invoice,i.tanggal,SUM(c.course_price) as total_price " +
                "FROM cart_course c join invoice i ON c.id_invoice = i.id_invoice where i.id_invoice = @IdInvoice group by c.id_invoice,i.tanggal";
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(query, con))
                {
                    cmd.Parameters.Clear();
                    cmd.Parameters.AddWithValue("@IdInvoice", InvoiceId);
                    try
                    {
                        con.Open();
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                string invoiceId = reader["id_invoice"].ToString() ?? string.Empty;
                                invoice = new InvoiceDetailDto
                                {
                                    Id = invoiceId,
                                    Date = Convert.ToDateTime(reader["tanggal"]),
                                    TotalPrice = Convert.ToInt32(reader["total_price"]),
                                    Courses = null
                                };
                            }
                        }

                    }
                    catch (Exception ex)
                    {
                        throw;
                    }
                    finally { con.Close(); }
                }
            }

            return invoice;
        }

        public List<CartInvoiceDto> GetCoursesForDetail(string InvoiceId)
        {
            List<CartInvoiceDto> invoices = new List<CartInvoiceDto>();

            string query = "SELECT c.course_name,c.course_category,c.date_course,c.course_price FROM cart_course c where c.id_invoice = @IdInvoice";

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(query,con))
                {
                    cmd.Parameters.Clear();
                    cmd.Parameters.AddWithValue("@IdInvoice", InvoiceId);
                    try
                    {
                        con.Open();
                        using(SqlDataReader reader = cmd.ExecuteReader()) { 
                            while (reader.Read())
                            {
                                CartInvoiceDto cart = new CartInvoiceDto
                                {
                                    CourseName = reader["course_name"].ToString() ?? string.Empty,
                                    CourseCategory = reader["course_category"].ToString() ?? string.Empty,
                                    CoursePrice = Convert.ToInt32(reader["course_price"]),
                                    CourseDate = Convert.ToDateTime(reader["date_course"])
                                };
                                invoices.Add(cart);
                            } 
                        }
                        
                    }catch (Exception ex)
                    {
                        throw;
                    }finally { 
                        con.Close(); 
                    }

                }
            }

            return invoices;
        }

        public string GetLastInvoiceNumber()
        {
            string lastInvoiceNumber = string.Empty;

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Open();

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = connection;
                cmd.CommandText = "SELECT TOP 1 id_invoice FROM invoice ORDER BY tanggal DESC";
                object result = cmd.ExecuteScalar();

                if (result != null)
                {
                    lastInvoiceNumber = result.ToString();
                }

                connection.Close();
            }

            return lastInvoiceNumber;
        }
    }
}
