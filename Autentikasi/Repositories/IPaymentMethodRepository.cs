﻿using System;
using System.Collections.Generic;
using otomobil.Dtos;
using otomobil.Models;

namespace otomobil.Repositories
{
    public interface IPaymentMethodRepository
    {
        List<PaymentMethod> GetAll();
        PaymentMethod GetById(Guid id);
        Guid AddPaymentMethod(CreatePaymentMethodDto paymentMethodDto);
        void Update(PaymentMethod paymentMethod);
        void Delete(Guid id);
    }
}
