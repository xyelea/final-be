﻿using System.Data.SqlClient;
using otomobil.Models;

namespace otomobil.Repositories
{
    public class CategoryRepository
    {
        private readonly IConfiguration _configuration;
        private readonly string ConnectionString;
        public CategoryRepository(IConfiguration configuration)
        {
            _configuration = configuration;
            this.ConnectionString = configuration.GetConnectionString("DefaultConnection");
        }

        public List<Category> GetCategories()
        {
            List<Category> categories = new List<Category>();

            string query = "SELECT * FROM Category";

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    try
                    {
                        connection.Open();

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {

                                Category category = new Category
                                {
                                    Id = Convert.ToInt32(reader["id_category"]),
                                    Name = reader["name"].ToString() ?? string.Empty,
                                    Description = reader["description"].ToString() ?? string.Empty, 
                                    Image = reader["image"].ToString() ?? string.Empty,
                                   
                                };
                                categories.Add(category);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw;
                    }
                    finally { 
                        connection.Close(); 
                    }
                }
            }

            return categories;
        }

        public Category? GetByName(string name)
        {
            Category? category = null;

            string query = $"SELECT * FROM category where name = @name";

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                using(SqlCommand cmd = new SqlCommand())
                {
                    try
                    {
                        cmd.Connection = connection;
                        cmd.Parameters.Clear();
                        cmd.CommandText = query;
                        cmd.Parameters.AddWithValue("@name", name);

                        connection.Open();
                        using(SqlDataReader reader = cmd.ExecuteReader()) 
                        {
                            while (reader.Read())
                            {
                                category = new Category
                                {
                                    Id = Convert.ToInt32(reader["id_category"]),
                                    Name = reader["name"].ToString() ?? string.Empty,
                                    Description = reader["description"].ToString() ?? string.Empty,
                                    Image = reader["image"].ToString() ?? string.Empty,
                                };
                            }
                        }

                    }catch (Exception ex)
                    {
                        throw;
                    }finally { connection.Close(); }
                }
            }
            return category;
        }
    }
}
