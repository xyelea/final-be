﻿using System.Data.SqlClient;
using otomobil.Models;

namespace otomobil.Repositories
{
    public class MyClassRepository
    {
        private readonly IConfiguration _configuration;
        private readonly string ConnectionString;
        public MyClassRepository(IConfiguration configuration)
        {
            _configuration = configuration;
            this.ConnectionString = configuration.GetConnectionString("DefaultConnection");
        }

        public List<Cart> GetAllClass(Guid userId)
        {
            List<Cart> myclass = new List<Cart>();

            string query = "SELECT * FROM cart_course WHERE id_user = @UserId AND id_invoice != ''";

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = con;
                    cmd.Parameters.Clear();
                    cmd.CommandText = query;
                    cmd.Parameters.AddWithValue("@UserId", userId);

                    try
                    {
                        con.Open();
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Cart cart = new Cart()
                                {
                                    Id = Convert.ToInt32(reader["id_cart_course"]),
                                    IdUser = Guid.Parse(reader["id_user"].ToString() ?? string.Empty),
                                    IdCourse = Convert.ToInt32(reader["id_course"]),
                                    CourseName = reader["course_name"].ToString() ?? string.Empty,
                                    CourseCategory = reader["course_category"].ToString() ?? string.Empty,
                                    CourseImage = reader["course_image"].ToString() ?? string.Empty,
                                    CoursePrice = Convert.ToInt32(reader["course_price"]),
                                    IdInvoice = reader["Id_invoice"].ToString() ?? string.Empty,
                                    Date = Convert.ToDateTime(reader["date_course"])
                                };
                                myclass.Add(cart);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }


                }
            }
            return myclass;
        }
    }
}
