﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using otomobil.Dtos;
using otomobil.Models;
using Microsoft.Extensions.Configuration;

namespace otomobil.Repositories
{
    public class PaymentMethodRepository : IPaymentMethodRepository
    {
        private readonly string _connectionString;

        public PaymentMethodRepository(IConfiguration configuration)
        {
            _connectionString = configuration.GetConnectionString("DefaultConnection");
        }

        public List<PaymentMethod> GetAll()
        {
            List<PaymentMethod> paymentMethods = new List<PaymentMethod>();

            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                string query = "SELECT id_payment_method, Name, Image, is_active FROM [payment_method]";
                SqlCommand command = new SqlCommand(query, connection);

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        PaymentMethod paymentMethod = new PaymentMethod
                        {
                            IdPaymentMethod = reader.GetGuid(reader.GetOrdinal("id_payment_method")),
                            Name = reader.GetString(reader.GetOrdinal("Name")),
                            Image = reader.GetString(reader.GetOrdinal("Image")),
                            IsActive = reader.GetBoolean(reader.GetOrdinal("is_active"))
                        };

                        paymentMethods.Add(paymentMethod);
                    }
                }
                connection.Close();
            }

            return paymentMethods;
        }


        public PaymentMethod GetById(Guid id)
        {
            PaymentMethod paymentMethod = null;

            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                string query = "SELECT id_payment_method, Name, Image, is_active FROM payment_method WHERE id_payment_method = @IdPaymentMethod";
                SqlCommand command = new SqlCommand(query, connection);
                command.Parameters.AddWithValue("@IdPaymentMethod", id);

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        paymentMethod = new PaymentMethod
                        {
                            IdPaymentMethod = reader.GetGuid(reader.GetOrdinal("id_payment_method")),
                            Name = reader.GetString(reader.GetOrdinal("Name")),
                            Image = reader.GetString(reader.GetOrdinal("Image")),
                            IsActive = reader.GetBoolean(reader.GetOrdinal("is_active"))
                        };
                    }
                }
                connection.Close();
            }

            return paymentMethod;
        }


        public Guid AddPaymentMethod(CreatePaymentMethodDto paymentMethodDto)
        {
            Guid id = Guid.NewGuid();

            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                string query = "INSERT INTO [payment_method] (id_payment_method, Name, Image, is_active) VALUES (@IdPaymentMethod, @Name, @Image, @IsActive)";
                SqlCommand command = new SqlCommand(query, connection);
                command.Parameters.AddWithValue("@IdPaymentMethod", id);
                command.Parameters.AddWithValue("@Name", paymentMethodDto.Name);
                command.Parameters.AddWithValue("@Image", paymentMethodDto.Image);
                command.Parameters.AddWithValue("@IsActive", paymentMethodDto.IsActive);

                command.ExecuteNonQuery();

                connection.Close();
            }

            return id;
        }

        public void Update(PaymentMethod paymentMethod)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                string query = "UPDATE payment_method SET Name = @Name, Image = @Image, is_active = @IsActive WHERE id_payment_method = @IdPaymentMethod";
                SqlCommand command = new SqlCommand(query, connection);
                command.Parameters.AddWithValue("@Name", paymentMethod.Name);
                command.Parameters.AddWithValue("@Image", paymentMethod.Image);
                command.Parameters.AddWithValue("@IsActive", paymentMethod.IsActive);
                command.Parameters.AddWithValue("@IdPaymentMethod", paymentMethod.IdPaymentMethod);

                command.ExecuteNonQuery();
                connection.Close();
            }
        }

        public void Delete(Guid id)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                string query = "DELETE FROM payment_method WHERE id_payment_method = @IdPaymentMethod";
                SqlCommand command = new SqlCommand(query, connection);
                command.Parameters.AddWithValue("@IdPaymentMethod", id);

                command.ExecuteNonQuery();
                connection.Close();
            }
        }


    }
}
