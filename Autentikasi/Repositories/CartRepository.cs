﻿using System.Data.SqlClient;
using System.Diagnostics.CodeAnalysis;
using otomobil.Models;

namespace otomobil.Repositories
{
    public class CartRepository
    {
        private readonly IConfiguration _configuration;
        private readonly string ConnectionString;

        public CartRepository(IConfiguration configuration)
        {
            _configuration = configuration;
            this.ConnectionString = configuration.GetConnectionString("DefaultConnection");
        }

        public bool Insert(Cart cart)
        {
            bool result = false;

            string query = "INSERT INTO cart_course VALUES (@IdUser,@IdCourse,@CourseName,@CourseCategory,@CourseImage,@CoursePrice,@IdInvoice,@Date)";

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using(SqlCommand cmd = con.CreateCommand())
                {
                    cmd.Connection = con;
                    cmd.Parameters.Clear();
                    cmd.CommandText = query;
                    cmd.Parameters.AddWithValue("@IdUser", cart.IdUser);
                    cmd.Parameters.AddWithValue("@IdCourse", cart.IdCourse);
                    cmd.Parameters.AddWithValue("@CourseName", cart.CourseName);
                    cmd.Parameters.AddWithValue("@CourseCategory", cart.CourseCategory);
                    cmd.Parameters.AddWithValue("@CourseImage", cart.CourseImage);
                    cmd.Parameters.AddWithValue("@CoursePrice", cart.CoursePrice);
                    cmd.Parameters.AddWithValue("@IdInvoice", cart.IdInvoice);
                    cmd.Parameters.AddWithValue("@Date", cart.Date);

                    try{
                        con.Open();
                        result = cmd.ExecuteNonQuery()>0;
                    }
                    catch(Exception ex)
                    {
                        throw;
                    }finally{
                        con.Close();
                    }

                }
            }
            return result;
        }

        public bool CheckIfUserAndDateExist(Guid userId,int courseId, DateTime date)
        {
            string datestring = date.ToString("yyyy-MM-dd");
            string query = "SELECT COUNT(*) FROM cart_course WHERE id_user = @UserId AND id_course = @CourseId AND date_course = @Date";
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = con;
                    cmd.Parameters.Clear();
                    cmd.CommandText = query;
                    cmd.Parameters.AddWithValue("@UserId", userId);
                    cmd.Parameters.AddWithValue("@CourseId", courseId);
                    cmd.Parameters.AddWithValue("@Date", datestring);

                    try
                    {
                        con.Open();
                        int count = (int)cmd.ExecuteScalar();
                        return count>0;
                    }
                    catch (Exception ex)
                    {
                        throw;
                    }finally { con.Close(); }
                    
                }
            }
        }

        public List<Cart> GetCart(Guid userId)
        {
            List<Cart> carts = new List<Cart>();

            string query = "SELECT * FROM cart_course WHERE id_user = @UserId AND id_invoice = ''";

            using ( SqlConnection con = new SqlConnection(ConnectionString))
            {
                using(SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = con;
                    cmd.Parameters.Clear();
                    cmd.CommandText= query;
                    cmd.Parameters.AddWithValue("@UserId", userId);

                    try
                    {
                        con.Open();
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Cart cart = new Cart()
                                {
                                    Id = Convert.ToInt32(reader["id_cart_course"]),
                                    IdUser = Guid.Parse(reader["id_user"].ToString() ?? string.Empty),
                                    IdCourse = Convert.ToInt32(reader["id_course"]),
                                    CourseName = reader["course_name"].ToString() ?? string.Empty,
                                    CourseCategory = reader["course_category"].ToString()??string.Empty,
                                    CourseImage = reader["course_image"].ToString()??string.Empty,
                                    CoursePrice = Convert.ToInt32(reader["course_price"]),
                                    IdInvoice = reader["Id_invoice"].ToString()??string.Empty,
                                    Date = Convert.ToDateTime(reader["date_course"])
                                };
                                carts.Add(cart);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw;
                    }finally
                    { 
                        con.Close(); 
                    }
                    
                    
                }
            }
            return carts;
        }

        public bool Delete(Guid UserId,int id)
        {
            bool result = false;
            string query = "DELETE FROM cart_course WHERE id_user = @userId AND id_cart_course = @id";
            using(SqlConnection con = new SqlConnection(ConnectionString))
            {
                using(SqlCommand cmd = con.CreateCommand())
                {
                    cmd.Connection = con;
                    cmd.Parameters.Clear();
                    cmd.CommandText = query;
                    cmd.Parameters.AddWithValue("@userId", UserId);
                    cmd.Parameters.AddWithValue("@id", id);
                    try
                    {
                        con.Open();
                        result = cmd.ExecuteNonQuery()>0;
                    }catch (Exception ex)
                    {
                        throw;
                    }finally { con.Close(); }
                }
            }
            return  result;
        }
    }
}
