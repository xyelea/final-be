﻿using otomobil.Dtos;
using System.Collections.Generic;

namespace otomobil.Repositories
{
    public interface IInvoiceAdminRepository
    {
        List<InvoiceAdminDto> GetInvoices();
        List<InvoiceAdminDto> GetInvoices(int limit);
    }
}
