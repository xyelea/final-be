﻿using otomobil.Dtos;
using System.Collections.Generic;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;

namespace otomobil.Repositories
{
    public class InvoiceAdminRepository : IInvoiceAdminRepository
    {
        private readonly string _connectionString;

        public InvoiceAdminRepository(IConfiguration configuration)
        {
            _connectionString = configuration.GetConnectionString("DefaultConnection");
        }

        public List<InvoiceAdminDto> GetInvoices()
        {
            var invoices = new List<InvoiceAdminDto>();

            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                var query = @"
                 SELECT 
                    ROW_NUMBER() OVER (ORDER BY i.tanggal DESC) AS No,
                    cc.id_invoice,
                    FORMAT(i.tanggal, 'dd MMMM yyyy') AS InvoiceTanggal,
                    COUNT(*) AS TotalCourse,
                    SUM(CAST(cc.course_price AS INT)) AS TotalCoursePrice
                FROM 
                    cart_course cc
                 JOIN 
                    invoice i ON cc.id_invoice = i.id_invoice
                WHERE 
                    cc.id_invoice IS NOT NULL
                GROUP BY 
                    cc.id_invoice, i.tanggal
                ORDER BY 
                    i.tanggal DESC;
            ";

                using (var command = new SqlCommand(query, connection))
                {
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var invoice = new InvoiceAdminDto
                            {
                                No = Convert.ToInt32(reader["No"]),
                                IdInvoice = Convert.ToString(reader["id_invoice"]),
                                InvoiceTanggal = Convert.ToString(reader["InvoiceTanggal"]),
                                TotalCourse = Convert.ToInt32(reader["TotalCourse"]),
                                TotalCoursePrice = Convert.ToDecimal(reader["TotalCoursePrice"])
                            };

                            invoices.Add(invoice);
                        }
                    }
                }
            }

            return invoices;
        }

        public List<InvoiceAdminDto> GetInvoices(int limit)
        {
            var invoices = new List<InvoiceAdminDto>();

            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                var query = $@"
                    SELECT TOP {limit}
                      ROW_NUMBER() OVER (ORDER BY i.tanggal DESC) AS No,
                    cc.id_invoice,
                    FORMAT(i.tanggal, 'dd MMMM yyyy') AS InvoiceTanggal,
                    COUNT(*) AS TotalCourse,
                    SUM(CAST(cc.course_price AS INT)) AS TotalCoursePrice
                FROM 
                    cart_course cc
                JOIN  
                    invoice i ON cc.id_invoice = i.id_invoice
                WHERE 
                    cc.id_invoice IS NOT NULL
                GROUP BY 
                    cc.id_invoice, i.tanggal
                ORDER BY 
                    i.tanggal DESC;
                ";

                using (var command = new SqlCommand(query, connection))
                {
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var invoice = new InvoiceAdminDto
                            {
                                No = Convert.ToInt32(reader["No"]),
                                IdInvoice = Convert.ToString(reader["id_invoice"]),
                                InvoiceTanggal = Convert.ToString(reader["InvoiceTanggal"]),
                                TotalCourse = Convert.ToInt32(reader["TotalCourse"]),
                                TotalCoursePrice = Convert.ToDecimal(reader["TotalCoursePrice"])
                            };

                            invoices.Add(invoice);
                        }
                    }
                }
            }

            return invoices;
        }
    }
}
