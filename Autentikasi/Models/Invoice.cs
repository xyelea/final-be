﻿namespace otomobil.Models
{
    public class Invoice
    {
        public string Id { get; set; } = string.Empty;

        public Guid UserId { get; set; }

        public Guid PaymentId { get; set; }

        public DateTime Date {get; set; }
    }
}
