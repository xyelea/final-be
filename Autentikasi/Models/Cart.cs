﻿namespace otomobil.Models
{
    public class Cart
    {
        public int Id { get; set; }

        public Guid IdUser { get; set; }

        public int IdCourse { get; set; }

        public string CourseName { get; set; }  = string.Empty;

        public string CourseCategory { get; set; } = string.Empty;

        public string CourseImage { get; set; } = string.Empty;

        public int CoursePrice { get; set; }

        public string IdInvoice { get; set; } = string.Empty;

        public DateTime Date { get; set;}

    }
}
