﻿namespace otomobil.Models
{
    public class OTP
    {
        public int Id { get; set; }
        public Guid UserId { get; set; }
        public string OTPCode { get; set; }
        public DateTime ExpiryDateTime { get; set; }
    }
}
