﻿namespace otomobil.Utils
{
    public class InvoiceNumberGenerator
    {
        public static string GenerateInvoiceNumber(int lastInvoiceNumber)
        {
            string invoicePrefix = "OTO";
            string invoiceNumber = (lastInvoiceNumber + 1).ToString("D5"); // Format lima digit (00001-99999)

            return invoicePrefix + invoiceNumber;
        }
    }
}
