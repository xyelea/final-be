﻿namespace otomobil.Dtos
{
    public class ResetPasswordDto
    {
        public string Email { get; set; }
    }
}
