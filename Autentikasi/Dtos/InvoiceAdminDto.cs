﻿namespace otomobil.Dtos
{
    public class InvoiceAdminDto
    {
        public int No { get; set; }
        public string IdInvoice { get; set; }
        public string InvoiceTanggal { get; set; }
        public int TotalCourse { get; set; }
        public decimal TotalCoursePrice { get; set; }
    }

}
