﻿namespace otomobil.Dtos
{
    public class CartInvoiceDto
    {
        public string CourseName { get; set; } = string.Empty;

        public string CourseCategory { get; set; } = string.Empty;

        public DateTime CourseDate { get; set; }

        public int CoursePrice { get; set; }
    }
}
