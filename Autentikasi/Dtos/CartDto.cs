﻿namespace otomobil.Dtos
{
    public class CartDto
    {
        public Guid IdUser { get; set; }

        public int IdCourse { get; set; }

        public string CourseName { get; set; } = string.Empty;

        public string CourseCategory { get; set; } = string.Empty;

        public string CourseImage { get; set; } = string.Empty;

        public int CoursePrice { get; set; }

        public DateTime Date { get; set; }
    }
}
