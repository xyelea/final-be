﻿namespace otomobil.Dtos
{
    public class InvoiceDto
    {
        public Guid UserId { get; set; }

        public Guid PaymentId { get; set; }

        public List<int> CartId { get; set; }
    }
}
