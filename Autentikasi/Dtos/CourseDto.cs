﻿namespace otomobil.Dtos
{
    public class CourseDto
    {
        public int Id { get; set; }
        public string Name { get; set; } = string.Empty;

        public int Price { get; set; }

        public string Image { get; set; } = string.Empty;

        public string Category { get; set; } = string.Empty;
    }
}
