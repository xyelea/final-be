﻿using otomobil.Models;

namespace otomobil.Dtos
{
    public class InvoiceDetailDto
    {
        public string Id { get; set; } = string.Empty;

        public DateTime Date { get; set; }

        public int TotalPrice { get; set; }

        public List<CartInvoiceDto>? Courses { get; set; }
    }
}
