﻿namespace otomobil.Dtos
{
    public class InvoiceDisplayDto
    {

        public string Id { get; set; } = string.Empty;

        public DateTime Date { get; set; } 

        public int TotalCourse { get; set; }

        public int TotalPrice { get; set; }
    }
}
