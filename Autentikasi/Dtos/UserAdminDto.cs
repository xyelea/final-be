﻿namespace otomobil.Dtos
{
        public class UserAdminDto
        {
            public string Username { get; set; } = string.Empty;
            public string Password { get; set; } = string.Empty;
            public string Email { get; set; } = string.Empty;
        }
         public class UserGetDto
        {
        public Guid Id { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Role { get; set; }
        public bool IsActive { get; set; }
        }
        public class UserUpdateDto
        {
            public string Username { get; set; }
            public string Email { get; set; }
            public string Password { get; set; }
            public string Role { get; set; }
            public bool IsActive { get; set; }
        }
        public class UserDeleteDto
        {
        public Guid Id { get; set; }
        }
}
