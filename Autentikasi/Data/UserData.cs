﻿using otomobil.Dtos;
using otomobil.Models;
using System.Data.SqlClient;

namespace otomobil.Data
{
    public class UserData
    {
        private readonly IConfiguration _configuration;
        private readonly string ConnectionString;

        public UserData(IConfiguration configuration)
        {
            _configuration = configuration;
            this.ConnectionString = _configuration.GetConnectionString("DefaultConnection");
        }

        public bool CreateUserAccount(User user, UserRole userRole)
        {
            bool result = false;

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                con.Open();

                SqlTransaction tran = con.BeginTransaction();

                try
                {
                    // First query
                    SqlCommand cmdUser = new SqlCommand();
                    cmdUser.Connection = con;
                    cmdUser.Transaction = tran;
                    cmdUser.Parameters.Clear();

                    cmdUser.CommandText = "INSERT INTO Users VALUES (@Id, @Username, @Password, @Email, @IsActivated)";
                    cmdUser.Parameters.AddWithValue("@Id", user.Id);
                    cmdUser.Parameters.AddWithValue("@Username", user.Username);
                    cmdUser.Parameters.AddWithValue("@Password", user.Password);
                    cmdUser.Parameters.AddWithValue("@Email", user.Email);
                    cmdUser.Parameters.AddWithValue("@IsActivated", user.IsActivated);


                    // Second Query
                    SqlCommand cmdRole = new SqlCommand();
                    cmdRole.Connection = con;
                    cmdRole.Transaction = tran;
                    cmdRole.Parameters.Clear();

                    cmdRole.CommandText = "INSERT INTO UserRoles VALUES (@UserId, @Role)";
                    cmdRole.Parameters.AddWithValue("@UserId", userRole.UserId);
                    cmdRole.Parameters.AddWithValue("@Role", userRole.Role);

                    var resultUser = cmdUser.ExecuteNonQuery();
                    var resultRole = cmdRole.ExecuteNonQuery();

                    tran.Commit();

                    result = true;

                }
                catch (Exception ex)
                {
                    tran.Rollback();
                    throw;
                }
                finally
                {
                    con.Close();
                }
            }

            return result;
        }

        public bool ChangePass(string email, PassChangeDto user)
        {
            bool result = false;

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                con.Open();

                try
                {
                    // First query
                    SqlCommand cmdUser = new SqlCommand();
                    cmdUser.Connection = con;
                    cmdUser.Parameters.Clear();

                    cmdUser.CommandText = "UPDATE Users SET Password = @Password WHERE Email = @Email";
  
                    cmdUser.Parameters.AddWithValue("@Email", email);
                    cmdUser.Parameters.AddWithValue("@Password", BCrypt.Net.BCrypt.HashPassword( user.Password));

                    var resultUser = cmdUser.ExecuteNonQuery();

                    result = true;

                }
                catch (Exception ex)
                {
                    throw ;
                }
                finally
                {
                    con.Close();
                }
            }

            return result;
        }
    


        public User? CheckUserLogin(string email)
        {
            User? user = null;

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = con;
                    cmd.CommandText = "SELECT * From Users WHERE email = @email";

                    cmd.Parameters.Clear();
                    cmd.Parameters.AddWithValue("@email", email);

                    con.Open();

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            user = new User
                            {
                                Id = Guid.Parse(reader["Id"].ToString() ?? string.Empty),
                                Username = reader["Username"].ToString() ?? string.Empty,
                                Password = reader["Password"].ToString() ?? string.Empty,
                                Email = reader["Email"].ToString() ?? string.Empty,
                                IsActivated = Convert.ToBoolean(reader["IsActivated"])

                            };
                        }
                    }

                    con.Close();
                }
            }

            return user;
        }

        public User? CheckUserActivated(string username)
        {
            User? user = null;

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = con;
                    cmd.CommandText = "SELECT * From Users WHERE Username = @username";

                    cmd.Parameters.Clear();
                    cmd.Parameters.AddWithValue("@username", username);

                    con.Open();

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            user = new User
                            {
                                Id = Guid.Parse(reader["Id"].ToString() ?? string.Empty),
                                Username = reader["Username"].ToString() ?? string.Empty,
                                Password = reader["Password"].ToString() ?? string.Empty,
                                Email = reader["Email"].ToString() ?? string.Empty,
                                IsActivated = Convert.ToBoolean(reader["IsActivated"])
                            };
                        }
                    }

                    con.Close();
                }
            }

            return user;

        }

        public bool CheckEmailRegistered(string email)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("SELECT COUNT(*) FROM Users WHERE Email = @Email", con))
                {
                    cmd.Parameters.AddWithValue("@Email", email);
                    con.Open();

                    int count = (int)cmd.ExecuteScalar();
                    return count > 0;
                }
            }
        }

        public UserReset? CheckEmailActivated(string email)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("SELECT * FROM Users WHERE Email = @Email", con))
                {
                    cmd.Parameters.AddWithValue("@Email", email);
                    con.Open();

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            return new UserReset
                            {
                              
                                Email = reader["Email"].ToString(),
                            
                            };
                        }
                    }
                }
            }

            return null;
        }


        public UserRole? CheckUserRole(Guid userId)
        {
            UserRole? userRole = null;

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = con;
                    cmd.CommandText = "SELECT * From UserRoles WHERE UserId = @userId";

                    cmd.Parameters.Clear();
                    cmd.Parameters.AddWithValue("@userId", userId);

                    con.Open();

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            userRole = new UserRole
                            {
                                Id = Convert.ToInt32(reader["Id"]),
                                UserId = Guid.Parse(reader["UserId"].ToString() ?? string.Empty),
                                Role = reader["Role"].ToString() ?? string.Empty,
                            };
                        }
                    }

                    con.Close();
                }
            }

            return userRole;
        }



        public List<User> GetAll()
        {
            List<User> users = new List<User>();

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Open();

                string query = "SELECT * FROM [Users]";
                SqlCommand command = new SqlCommand(query, connection);

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        User user = new User
                        {
                            Id = reader.GetGuid(reader.GetOrdinal("Id")),
                            Username = reader.GetString(reader.GetOrdinal("Username")),
                            Password = reader.GetString(reader.GetOrdinal("Password")),
                        };

                        users.Add(user);
                    }
                }
            }

            return users;
        }

        public bool ActivateUser(Guid userId)
        {
            bool result = true;

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.Parameters.Clear();

                cmd.CommandText = "UPDATE Users SET IsActivated = 1 WHERE Id = @Id";
                cmd.Parameters.AddWithValue("@Id", userId);

                con.Open();
                result = cmd.ExecuteNonQuery() > 0 ? true : false;
                con.Close();
            }

            return result;
        }

        public bool ResetUserPass(Guid userId, string password)
        {
            bool result = true;

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.Parameters.Clear();

                cmd.CommandText = "UPDATE Users SET Password = @password WHERE Id = @Id";
                cmd.Parameters.AddWithValue("@Id", userId);
                cmd.Parameters.AddWithValue("@password", password);

                con.Open();
                result = cmd.ExecuteNonQuery() > 0 ? true : false;
                con.Close();
            }

            return result;
        }


        public bool SaveResetToken(string email, string resetToken)
        {
            bool result = false;

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                con.Open();

                SqlTransaction tran = con.BeginTransaction();

                try
                {
                    // Insert reset token into OTP table
                    SqlCommand cmdOtp = new SqlCommand();
                    cmdOtp.Connection = con;
                    cmdOtp.Transaction = tran;
                    cmdOtp.Parameters.Clear();

                    cmdOtp.CommandText = "INSERT INTO OTP (UserId, OTPCode, ExpiryDateTime) VALUES (@UserId, @OTPCode, @ExpiryDateTime)";
                    cmdOtp.Parameters.AddWithValue("@UserId", GetUserIdByEmail(email)); // You need to implement GetUserIdByEmail method to retrieve the user's ID based on the email
                    cmdOtp.Parameters.AddWithValue("@OTPCode", resetToken);
                    cmdOtp.Parameters.AddWithValue("@ExpiryDateTime", DateTime.UtcNow.AddDays(1));

                    int resultOtp = cmdOtp.ExecuteNonQuery();

                    tran.Commit();

                    result = true;
                }
                catch (Exception ex)
                {
                    tran.Rollback();
                    throw;
                }
                finally
                {
                    con.Close();
                }
            }

            return result;
        }

        public Guid GetUserIdByEmail(string email)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                con.Open();

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandText = "SELECT Id FROM Users WHERE Email = @Email";
                cmd.Parameters.AddWithValue("@Email", email);

                object result = cmd.ExecuteScalar();

                if (result != null && result != DBNull.Value)
                {
                    return (Guid)result;
                }
            }

            throw new Exception("User not found"); // Ganti dengan penanganan yang sesuai jika pengguna tidak ditemukan
        }


    }
}
