﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Data;

namespace Autentikasi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RoleBasedController : ControllerBase
    {
        [Authorize(Roles = "admin")]
        [HttpGet("getForAdmin")]
        public IActionResult GetForAdmin()
        {
            return Ok("Admin");
        }

        [Authorize(Roles = "admin,user")]
        [HttpGet("getForAll")]
        public IActionResult getForAll()
        {
            return Ok("Admin & User");
        }

    }
}
