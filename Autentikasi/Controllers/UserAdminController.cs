﻿using otomobil.Services;
using otomobil.Dtos;
using otomobil.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;

namespace otomobil.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserAdminController : ControllerBase
    {
        private readonly IUserService _userService;

        public UserAdminController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpPost]
        public IActionResult Post([FromBody] UserAdminDto userDto)
        {
            try
            {
                bool result = _userService.CreateUser(userDto);

                if (result)
                {
                    return StatusCode(201, "User added successfully.");
                }

                return StatusCode(500, "Error");
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpGet("{id}")]
        public IActionResult GetById(Guid id)
        {
            try
            {
                var user = _userService.GetUserById(id);

                if (user == null)
                {
                    return NotFound();
                }

                return Ok(user);
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Error");
            }
        }

        [HttpGet("GetAllUsers")]
        public IActionResult GetAll()
        {
            try
            {
                var users = _userService.GetAllUsers();

                return Ok(users);
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Error");
            }
        }

        [HttpGet]
        public IActionResult GetAll([FromQuery] int limit = 5)
        {
            try
            {
                var users = _userService.GetAllUsers(limit);

                return Ok(users);
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Error");
            }
        }

        [HttpPut("{id}")]
        public IActionResult UpdateUser(Guid id, [FromBody] UserUpdateDto userDto)
        {
            try
            {
                bool result = _userService.UpdateUser(id, userDto);

                if (result)
                {
                    return Ok("User updated successfully.");
                }

                return StatusCode(500, "Error updating user.");
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Error updating user.");
            }
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteUser(Guid id)
        {
            try
            {
                bool result = _userService.DeleteUser(id);

                if (result)
                {
                    return Ok("User deleted successfully.");
                }

                return StatusCode(500, "Error deleting user.");
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Error deleting user.");
            }
        }
    }
}
