﻿using System.Runtime.CompilerServices;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using otomobil.Dtos;
using otomobil.Models;
using otomobil.Services;

namespace otomobil.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class InvoiceController : ControllerBase
    {
        private readonly InvoiceService _InvoiceService;

        public InvoiceController(InvoiceService invoiceService)
        {
            _InvoiceService = invoiceService;
        }

        [HttpPost]

        public IActionResult CreateInvoice([FromBody]InvoiceDto invoiceDto)
        {
            try
            {
                if (invoiceDto == null)
                    return BadRequest("Data should be inserted");
                bool result = _InvoiceService.CreateInvoice(invoiceDto);
                if (result) 
                    return Ok(result);
                return BadRequest("gagal");

            }catch (Exception ex)
            {
                return Problem(ex.Message);
            }
        }

        [HttpGet]
        
        public IActionResult GetAllInvoice(Guid UserId) 
        {
            try
            {
                var invoice = _InvoiceService.GetInvoices(UserId);
                return Ok(invoice);
            }catch (Exception ex)
            {
                return Problem(ex.Message);
            }
        }

        [HttpGet("detail")]

        public IActionResult GetDetailInvoice(string InvoiceId)
        {
            try
            {
                var invoiceDetail = _InvoiceService.GetDetailInvoice(InvoiceId);
                return Ok(invoiceDetail);
            }catch(Exception ex)
            {
                return Problem(ex.Message);
            }
        }
    }
}
