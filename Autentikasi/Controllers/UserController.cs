﻿using otomobil.Data;
using otomobil.Dtos;
using otomobil.Emails;
using otomobil.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;



namespace otomobil.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly UserData userData;
        private readonly IConfiguration _configuration;
        private readonly EmailService _emailService;


        public UserController(UserData userData, IConfiguration configuration, EmailService emailService)
        {
            this.userData = userData;
            _configuration = configuration;
            _emailService = emailService;
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] UserDto userDto)
        {

            // Cek apakah email sudah terdaftar
            bool isEmailRegistered = userData.CheckEmailRegistered(userDto.Email);
            if (isEmailRegistered)
            {
                return StatusCode(400, "Email already registered");
            }

            try
            {
                User user = new User
                {
                    Id = Guid.NewGuid(),
                    Username = userDto.Username,
                    Password = BCrypt.Net.BCrypt.HashPassword(userDto.Password),
                    Email = userDto.Email,
                    IsActivated = false
                };

                UserRole userRole = new UserRole
                {
                    UserId = user.Id,
                    Role = userDto.Role
                };

                bool result = userData.CreateUserAccount(user, userRole);

                if (result)
                {
                    bool resultMail = await SendEmail(user);
                    return StatusCode(201, user.Id);
                }

                return StatusCode(500, "Error");
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Error");
            }
        }

        [HttpPut("changepass")]
        public IActionResult ChangePass(string email, [FromBody] PassChangeDto user)
        {
            try
            {
                bool result = userData.ChangePass(email, user );

                if (result)
                {
                    return Ok("User updated successfully.");
                }

                return StatusCode(500, "Error updating user.");
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Error updating user.");
            }
        }

        [HttpPost("login")]
        public IActionResult Post([FromBody] LoginRequestDto requestDto)
        {
            if (requestDto == null)
                return BadRequest("invalid credential request");

            if (string.IsNullOrEmpty(requestDto.Email) || string.IsNullOrEmpty(requestDto.Password))
                return BadRequest("invalid credential request");

            User? user = userData.CheckUserLogin(requestDto.Email);

            if (user == null)
                return Unauthorized("You don't have authorization");

            if (!user.IsActivated)
            {
                return Unauthorized("You have not activated your account");
            }

            UserRole? userRole = userData.CheckUserRole(user.Id);

            bool passwordVerification = BCrypt.Net.BCrypt.Verify(requestDto.Password, user?.Password);

            if (user != null && !passwordVerification)
                return BadRequest("invalid credential request");

            var secretKey = _configuration.GetSection("JwTConfig:Key").Value;
            var jwtKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(secretKey));

            var claims = new Claim[]
            {
                new Claim(JwtRegisteredClaimNames.Sub, user?.Id.ToString()),
                new Claim(ClaimTypes.Name, user?.Email),
                new Claim(ClaimTypes.Role, userRole.Role)
            };

            var signingCredential = new SigningCredentials(jwtKey, SecurityAlgorithms.HmacSha256Signature);

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims),
                Expires = DateTime.UtcNow.AddDays(20),
                SigningCredentials = signingCredential
            };

            var tokenHandler = new JwtSecurityTokenHandler();

            var securityToken = tokenHandler.CreateToken(tokenDescriptor);

            string token = tokenHandler.WriteToken(securityToken);

            return Ok(new LoginResponseDto
            {
                Token = token
            });

        }

        [HttpGet("ActivateUser")]
        public IActionResult ActivateUser(Guid id, string username)
        {
            try
            {
                User? user = userData.CheckUserActivated(username);

                if (user == null)
                {
                    return BadRequest("No User found");
                }

                if (user.IsActivated)
                {
                    return BadRequest("User has been activated");
                }

                bool result = userData.ActivateUser(id);

                if (result)
                {
                    string emailConfirmationUrl = _configuration["RedirectHost:EmainConfirmation"];
                    return Redirect(emailConfirmationUrl);
                }
                else
                {
                    return Problem("Failed");
                }
            }
            catch (Exception ex)
            {
                return Problem(ex.Message);
            }
        }

        [HttpGet("Reset")]
        public IActionResult Reset(Guid id, string email)
        {
            var param = new Dictionary<string, string?>
    {
        { "email", email }
    };
            string ResetUrl = _configuration["RedirectHost:ResetUrl"];
            string redirectUrl = QueryHelpers.AddQueryString(ResetUrl, param);
            return Redirect(redirectUrl);
        }


        // endpoint post 
        [HttpPost("resetpass")]
        public async Task<IActionResult> ResetPassUser([FromBody] UserReset user)
        {
            try
            {
                UserReset? existingUser = userData.CheckEmailActivated(user.Email);

                if (existingUser != null)
                {
                    bool resultMail = await SendMailReset(existingUser);
                    return StatusCode(201, existingUser.Email);
                }

                return BadRequest("User not found");
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Error");
            }
        }

        private async Task<bool> SendMailReset( UserReset user)
        {
            if (user.Email == null)
            {
                return false;
            }
           

            if (string.IsNullOrEmpty(user.Email))
            {
                return false;
            }

            List<string> to = new List<string>();
            to.Add(user.Email);

            string subject = "Reset Password";


            var param = new Dictionary<string, string?>
            {
                { "email", user.Email}
            };

            string url = _configuration["RedirectHost:CallbackUrl"];

            string callBackUrl = QueryHelpers.AddQueryString(url, param);

            string body = _emailService.GetEmailTemplate("ResetPassword", new Emails.Template.EmailActivation
            {
                Email = user.Email,
                Link = callBackUrl
            });

            EmailModel model = new EmailModel(to, subject, body);

            bool result = await _emailService.SendAsync(model, new CancellationToken());

            return result;
        }

        private async Task<bool> SendEmail(User user)
        {
            if (user == null)
            {
                return false;
            }

            if (string.IsNullOrEmpty(user.Email))
            {
                return false;
            }

            List<string> to = new List<string>();
            to.Add(user.Email);

            string subject = "Account Activation";

            var param = new Dictionary<string, string?>
            {
                { "id", user.Id.ToString() },
                { "username", user.Username }
            };

            string url = _configuration["RedirectHost:ActivateUrl"];
            string callBackUrl = QueryHelpers.AddQueryString(url, param);

            //string body = "Please confirm your account by clicking <a href=\"" + callBackUrl + "\">here</a>";

            string body = _emailService.GetEmailTemplate("EmailActivation", new Emails.Template.EmailActivation
            {
                Email = user.Email,
                Link = callBackUrl
            });

            EmailModel model = new EmailModel(to, subject, body);

            bool result = await _emailService.SendAsync(model, new CancellationToken());

            return result;
        }



    }
}
