﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using otomobil.Services;

namespace otomobil.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MyClassController : ControllerBase
    {
        private readonly MyClassService _myClassService;
        public MyClassController(MyClassService myClassService)
        {
            _myClassService = myClassService;
        }

        [HttpGet]
        public IActionResult GetAll(Guid UserId)
        {
            try
            {
                var myclass = _myClassService.GetAll(UserId);
                return Ok(myclass);
            }catch
            (Exception ex)
            {
                return BadRequest(ex.Message);  
            }
        }
    }
}
