﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using otomobil.Services;

namespace otomobil.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoryController : ControllerBase
    {
        private readonly CategoryService _categoryService;
        public CategoryController(CategoryService categoryService)
        {
            _categoryService = categoryService;
        }

        [HttpGet]
        public IActionResult GetCategories()
        {
            try
            {
                var categories = _categoryService.GetCategories();   
                return Ok(categories);
            }catch (Exception ex)
            {
                return Problem(ex.Message);
            }
        }

        [HttpGet("detail")]
        public IActionResult GetCategory(string name)
        {
            try
            {
                var category = _categoryService.GetCategory(name);
                if (category == null)
                {
                    return NotFound();
                }
                    
                return Ok(category);
            }
            catch (Exception ex)
            {
                return Problem(ex.Message);
            }
        }
    }
}
