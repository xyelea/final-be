﻿using otomobil.Dtos;
using otomobil.Services;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace otomobil.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class InvoiceAdminController : ControllerBase
    {
        private readonly IInvoiceAdminService _invoiceService;

        public InvoiceAdminController(IInvoiceAdminService invoiceService)
        {
            _invoiceService = invoiceService;
        }

        [HttpGet("GetAllInvoice")]
        public ActionResult<List<InvoiceAdminDto>> GetAllInvoice()
        {
            var invoices = _invoiceService.GetInvoices();
            return Ok(invoices);
        }

        [HttpGet()]
        public ActionResult<List<InvoiceAdminDto>> GetInvoiceLimit(int limit)
        {
            var invoices = _invoiceService.GetInvoices(limit);
            return Ok(invoices);
        }
    }
}
