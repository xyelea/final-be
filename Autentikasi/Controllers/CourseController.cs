﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using otomobil.Services;

namespace otomobil.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CourseController : ControllerBase
    {
        private readonly CourseService _courseService;

        public CourseController(CourseService courseService)
        {
            _courseService = courseService;
        }

        [HttpGet]
        public IActionResult GetCourses() 
        {
            try
            {
                var course = _courseService.GetCourses();
                return Ok(course);
            }catch(Exception ex)
            {
                return Problem(ex.Message);
            }
        }

        [HttpGet("getAllOther")]
        public IActionResult GetCoursesOther(int id)
        {
            try
            {
                var course = _courseService.GetCoursesOther(id);
                return Ok(course);
            }
            catch (Exception ex)
            {
                return Problem(ex.Message);
            }
        }

        [HttpGet("getAllByCategory")]
        public IActionResult GetCoursesByCategory(string name)
        {
            try
            {
                var course = _courseService.GetCoursesByCategory(name);
                if (course == null)
                {
                    return NotFound();
                }
                return Ok(course);
            }
            catch (Exception ex)
            {
                return Problem(ex.Message);
            }
        }

        [HttpGet("detail")]
        public IActionResult GetCourse(int id)
        {
            try
            {
                var course = _courseService.GetCourse(id);
                if (course == null)
                {
                    return NotFound();
                }

                return Ok(course);
            }
            catch (Exception ex)
            {
                return Problem(ex.Message);
            }
        }
    }
}
