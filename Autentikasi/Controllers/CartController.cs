﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using otomobil.Dtos;
using otomobil.Models;
using otomobil.Services;

namespace otomobil.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CartController : ControllerBase
    {
        private readonly CartService _cartService;

        public CartController(CartService cartService)
        {
            _cartService = cartService;
        }

        [HttpPost]
        public IActionResult Post([FromBody]CartDto cartDto)
        {
            try
            {
                if (cartDto == null)
                    return BadRequest("Data should be inserted");

                

                bool result = _cartService.PostCart(cartDto);
                if (result)
                    return Ok();
                return BadRequest("A cart with the same id course and date already exists.");



            }
            catch (Exception ex)
            {
                return Problem(ex.Message);
            }

        }

        [HttpGet]
        public IActionResult Get(Guid userId)
        {
            try
            {
                var cart = _cartService.GetAll(userId);
                return Ok(cart);
            }
            catch (Exception ex)
            {
                return Problem($"Could not find {userId}");
            }
            

        }

        [HttpDelete] 
        public IActionResult Delete(Guid userId,int Id) 
        {
            try
            {
                bool result = _cartService.Delete(userId,Id);
                if (result) return Ok();
                return Problem("error");
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
