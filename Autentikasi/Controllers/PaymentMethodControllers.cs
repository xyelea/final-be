﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using otomobil.Models;
using otomobil.Services;
using otomobil.Dtos;
using otomobil.Services;


namespace otomobil.Controllers
{
    [ApiController]
    [Route("api/paymentmethods")]
    public class PaymentMethodController : ControllerBase
    {
        private readonly IPaymentMethodService _paymentMethodService;

        public PaymentMethodController(IPaymentMethodService paymentMethodService)
        {
            _paymentMethodService = paymentMethodService;
        }

        [HttpGet]
        public IActionResult GetAllPaymentMethods()
        {
            List<PaymentMethod> paymentMethods = _paymentMethodService.GetAllPaymentMethods();
            return Ok(paymentMethods);
        }

        [HttpGet("{id}")]
        public IActionResult GetPaymentMethodById(Guid id)
        {
            PaymentMethod paymentMethod = _paymentMethodService.GetPaymentMethodById(id);
            if (paymentMethod == null)
            {
                return NotFound();
            }

            return Ok(paymentMethod);
        }

        [HttpPost]
        public IActionResult AddPaymentMethod(CreatePaymentMethodDto paymentMethodDto)
        {
            try
            {
                _paymentMethodService.AddPaymentMethod(paymentMethodDto);
                return Ok("Payment added successfully.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPut("{id}")]
        public IActionResult UpdatePaymentMethod(Guid id, PaymentMethod paymentMethod)
        {
            if (id != paymentMethod.IdPaymentMethod)
            {
                return BadRequest();
            }

            _paymentMethodService.UpdatePaymentMethod(paymentMethod);
            return Ok("Payment updated successfully.");
        }

        [HttpDelete("{id}")]
        public IActionResult DeletePaymentMethod(Guid id)
        {
            _paymentMethodService.DeletePaymentMethod(id);
            return NoContent();
        }
    }
}
