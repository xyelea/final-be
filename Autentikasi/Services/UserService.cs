﻿using System;
using System.Collections.Generic;
using otomobil.Dtos;
using otomobil.Models;
using otomobil.Repositories;
using otomobil.Services;

namespace otomobil.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;

        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public bool CreateUser(UserAdminDto userDto)
        {
            try
            {

                string email = userDto.Email.ToLower();


                if (_userRepository.IsUsernameExists(userDto.Username))
                {
                    throw new Exception("Username already exists.");
                }


                if (_userRepository.IsEmailExists(email))
                {
                    throw new Exception("Email already exists.");
                }

                User user = new User
                {
                    Id = Guid.NewGuid(),
                    Username = userDto.Username,
                    Password = BCrypt.Net.BCrypt.HashPassword(userDto.Password),
                    Email = email,
                    IsActivated = true
                };

                UserRole userRole = new UserRole
                {
                    UserId = user.Id,
                    Role = "member"
                };

                bool result = _userRepository.CreateUserAccount(user, userRole);

                return result;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public UserGetDto GetUserById(Guid id)
        {
            return _userRepository.GetUserById(id);
        }

        public List<UserGetDto> GetAllUsers()
        {
            return _userRepository.GetAllUsers();
        }

        public List<UserGetDto> GetAllUsers(int limit)
        {
            return _userRepository.GetAllUsers(limit);
        }
        public bool UpdateUser(Guid id, UserUpdateDto userDto)
        {
            return _userRepository.UpdateUser(id, userDto);
        }

        public bool DeleteUser(Guid id)
        {
            return _userRepository.DeleteUser(id);
        }
    }
}
