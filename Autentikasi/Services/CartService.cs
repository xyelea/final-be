﻿using otomobil.Dtos;
using otomobil.Models;
using otomobil.Repositories;

namespace otomobil.Services
{
    public class CartService
    {
        private readonly CartRepository _cartRepository;
        public CartService(CartRepository cartRepository)
        {
            _cartRepository = cartRepository;
        }

        public bool PostCart(CartDto cartDto)
        {
            
            bool isUserAndDateExist = CheckIfUserAndDateExist(cartDto.IdUser,cartDto.IdCourse, cartDto.Date);
            if (isUserAndDateExist)
            {
                return false;
            }
            Cart cart = new Cart()
            {
                IdUser = cartDto.IdUser,
                IdCourse = cartDto.IdCourse,
                CourseName = cartDto.CourseName,
                CourseCategory = cartDto.CourseCategory,
                CourseImage = cartDto.CourseImage,
                CoursePrice = cartDto.CoursePrice,
                IdInvoice = string.Empty,
                Date = cartDto.Date,
            };
            return _cartRepository.Insert(cart);
        }

        private bool CheckIfUserAndDateExist(Guid userId,int courseId,DateTime date)
        {
            return _cartRepository.CheckIfUserAndDateExist(userId,courseId, date);
        }

        public List<Cart> GetAll(Guid userId)
        {
            var cart = _cartRepository.GetCart(userId);
            return cart;
        }

        public bool Delete(Guid userId,int id)
        {
            return _cartRepository.Delete(userId,id);
        }
    }
}
