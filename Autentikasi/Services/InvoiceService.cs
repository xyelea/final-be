﻿using otomobil.Dtos;
using otomobil.Models;
using otomobil.Repositories;
using otomobil.Utils;

namespace otomobil.Services
{
    public class InvoiceService
    {
        private readonly InvoiceRepository _invoiceRepository;

        public InvoiceService(InvoiceRepository invoiceRepository)
        {
            _invoiceRepository = invoiceRepository;
        }

        public bool CreateInvoice(InvoiceDto invoiceDto)
        {
            var isCartExist = _invoiceRepository.CheckIfCartExist(invoiceDto.UserId);
            if (!isCartExist)
            {
                return false;
            }

            var lastInvoice = _invoiceRepository.GetLastInvoiceNumber();
            int lastInvoiceNumber = 0;
            if (lastInvoice != string.Empty)
            {
                lastInvoiceNumber = int.Parse(lastInvoice.Substring(3));
            }

            string newInvoiceNumber = InvoiceNumberGenerator.GenerateInvoiceNumber(lastInvoiceNumber);
            Invoice invoice = new Invoice()
            {
                Id = newInvoiceNumber,
                UserId = invoiceDto.UserId,
                PaymentId = invoiceDto.PaymentId,
                Date = DateTime.Now
            };

           

            return _invoiceRepository.CreateInvoice(invoice,invoiceDto.CartId);
        }

        public List<InvoiceDisplayDto> GetInvoices(Guid UserId)
        {
            return _invoiceRepository.GetAllInvoice(UserId);
        }

        public InvoiceDetailDto? GetDetailInvoice(string InvoiceId)
        {
            var invoice = _invoiceRepository.GetDetailInvoice(InvoiceId);
            invoice.Courses = _invoiceRepository.GetCoursesForDetail(InvoiceId);
        
            return invoice;
        }
    }
}
