﻿using System;
using System.Collections.Generic;
using otomobil.Dtos;
using otomobil.Models;

namespace otomobil.Services
{
    public interface IPaymentMethodService
    {
        List<PaymentMethod> GetAllPaymentMethods();
        PaymentMethod GetPaymentMethodById(Guid id);
        Guid AddPaymentMethod(CreatePaymentMethodDto paymentMethodDto);
        void UpdatePaymentMethod(PaymentMethod paymentMethod);
        void DeletePaymentMethod(Guid id);
    }
}
