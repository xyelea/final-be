﻿using otomobil.DTOs;
using otomobil.Models;
using otomobil.Repositories;

namespace otomobil.Services
{
    public class CategoryService
    {
        private readonly CategoryRepository _categoryRepository;

        public CategoryService(CategoryRepository categoryRepository)
        {
            _categoryRepository = categoryRepository;   
        }

        public List<CategoryDto> GetCategories()
        {

            List<CategoryDto> categoriesDto = new List<CategoryDto>();

            var categories = _categoryRepository.GetCategories();
            foreach (var category in categories)
            {
                CategoryDto categoryDto = new CategoryDto();
                categoryDto.Id = category.Id;
                categoryDto.Name = category.Name;
                categoryDto.Image = category.Image;
                categoriesDto.Add(categoryDto);
            }

            return categoriesDto;
           
        }

        public CategoryDetailsDto GetCategory(string name)
        {
            CategoryDetailsDto categoryDetailsDto = new CategoryDetailsDto();

            var category = _categoryRepository.GetByName(name);
            if (category != null)
            {
                categoryDetailsDto.Id = category.Id;
                categoryDetailsDto.Name = category.Name;
                categoryDetailsDto.Description = category.Description;
            }
            else
            {
                return null;
            }
            return categoryDetailsDto;
        }
    }
}
