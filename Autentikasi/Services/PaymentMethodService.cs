﻿using System;
using System.Collections.Generic;
using otomobil.Dtos;
using otomobil.Models;
using otomobil.Repositories;

namespace otomobil.Services
{
    public class PaymentMethodService : IPaymentMethodService
    {
        private readonly IPaymentMethodRepository _paymentMethodRepository;

        public PaymentMethodService(IPaymentMethodRepository paymentMethodRepository)
        {
            _paymentMethodRepository = paymentMethodRepository;
        }

        public List<PaymentMethod> GetAllPaymentMethods()
        {
            return _paymentMethodRepository.GetAll();
        }

        public PaymentMethod GetPaymentMethodById(Guid id)
        {
            return _paymentMethodRepository.GetById(id);
        }

        public Guid AddPaymentMethod(CreatePaymentMethodDto paymentMethodDto)
        {
            return _paymentMethodRepository.AddPaymentMethod(paymentMethodDto);
        }

        public void UpdatePaymentMethod(PaymentMethod paymentMethod)
        {
            _paymentMethodRepository.Update(paymentMethod);
        }

        public void DeletePaymentMethod(Guid id)
        {
            _paymentMethodRepository.Delete(id);
        }
    }
}
