﻿using otomobil.Models;
using otomobil.Repositories;

namespace otomobil.Services
{
    public class MyClassService
    {
        private readonly MyClassRepository _myClassRepository;
        public MyClassService(MyClassRepository myClassRepository)
        {
            _myClassRepository = myClassRepository;
        }
        public List<Cart> GetAll(Guid userId)
        {
            var myclass = _myClassRepository.GetAllClass(userId);
            return myclass;
        }
    }
}
