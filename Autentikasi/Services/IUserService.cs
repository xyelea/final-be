﻿using System;
using System.Collections.Generic;
using otomobil.Dtos;
using otomobil.Models;

namespace otomobil.Services
{
        public interface IUserService
        {
            bool CreateUser(UserAdminDto userDto);
            UserGetDto GetUserById(Guid id);
            List<UserGetDto> GetAllUsers();
            List<UserGetDto> GetAllUsers(int limit);
            bool UpdateUser(Guid id, UserUpdateDto userDto);
            bool DeleteUser(Guid id);
    }
}
