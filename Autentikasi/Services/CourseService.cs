﻿using otomobil.Dtos;
using otomobil.Models;
using otomobil.Repositories;

namespace otomobil.Services
{
    public class CourseService
    {
        private readonly CourseRepository _courseRepository;

        public CourseService(CourseRepository courseRepository)
        {
            _courseRepository = courseRepository;            
        }

        public List<CourseDto> GetCourses()
        {
            List<CourseDto> coursesDto = new List<CourseDto>();

            var courses = _courseRepository.GetCourses();
            foreach ( var course in courses )
            {
                CourseDto courseDto = new CourseDto();
                courseDto.Id = course.Id;
                courseDto.Name = course.Name;
                courseDto.Price = course.Price;
                courseDto.Image = course.Image;
                courseDto.Category = course.Category.Name;
                coursesDto.Add( courseDto );
            }

            return coursesDto;
        }

        public List<CourseDto> GetCoursesOther(int id)
        {
            List<CourseDto> coursesDto = new List<CourseDto>();

            var courses = _courseRepository.GetCoursesOther(id);
            
                foreach (var course in courses)
                {
                    CourseDto courseDto = new CourseDto();
                    courseDto.Id = course.Id;
                    courseDto.Name = course.Name;
                    courseDto.Price = course.Price;
                    courseDto.Image = course.Image;
                    courseDto.Category = course.Category.Name;
                    coursesDto.Add(courseDto);
                }
            return coursesDto;
        }

        public List<CourseDto> GetCoursesByCategory(string name)
        {
            List<CourseDto> coursesDto = new List<CourseDto>();

            var courses = _courseRepository.GetCoursesByCategory(name);
            if (courses != null)
            {
                foreach (var course in courses)
                {
                    CourseDto courseDto = new CourseDto();
                    courseDto.Id = course.Id;
                    courseDto.Name = course.Name;
                    courseDto.Price = course.Price;
                    courseDto.Image = course.Image;
                    courseDto.Category = course.Category.Name;
                    coursesDto.Add(courseDto);
                }
            }
            else
            {
                return null;
            }
            
            return coursesDto;
        }

        public CourseDetailDto? GetCourse(int id)
        {
            CourseDetailDto courseDetailDto = new CourseDetailDto();

            var course = _courseRepository.GetById( id );
            if ( course != null )
            {
                courseDetailDto.Id = course.Id;
                courseDetailDto.Name = course.Name;
                courseDetailDto.Price = course.Price;
                courseDetailDto.Image = course.Image;
                courseDetailDto.Description = course.Description;
                courseDetailDto.Category = course.Category.Name;
            }
            else
            {
                return null;
            }

            return courseDetailDto;
        }
    }
}
