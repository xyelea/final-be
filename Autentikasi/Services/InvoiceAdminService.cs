﻿using otomobil.Dtos;
using otomobil.Repositories;
using System.Collections.Generic;

namespace otomobil.Services
{
    public class InvoiceAdminService : IInvoiceAdminService
    {
        private readonly IInvoiceAdminRepository _invoiceRepository;

        public InvoiceAdminService(IInvoiceAdminRepository invoiceRepository)
        {
            _invoiceRepository = invoiceRepository;
        }

        public List<InvoiceAdminDto> GetInvoices()
        {
            return _invoiceRepository.GetInvoices();
        }

        public List<InvoiceAdminDto> GetInvoices(int limit)
        {
            return _invoiceRepository.GetInvoices(limit);
        }
    }
}
