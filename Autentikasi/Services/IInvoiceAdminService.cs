﻿using otomobil.Dtos;

namespace otomobil.Services
{
    public interface IInvoiceAdminService
    {
        List<InvoiceAdminDto> GetInvoices();
        List<InvoiceAdminDto> GetInvoices(int limit);
    }

}
