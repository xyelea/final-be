﻿namespace otomobil.Emails.Template
{
    public class EmailActivation
    {
        public string? Email { get; set; }
        public string? Link { get; set; }

    }
}
