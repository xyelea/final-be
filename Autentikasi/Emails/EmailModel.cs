﻿namespace otomobil.Emails
{
    public class EmailModel
    {
        //Receiver
        public List<string> To { get; }
        public List<string> Cc { get; }
        public List<string> Bcc { get; }

        //Sender
        public string? From { get; }
        public string? FromDisplayName { get; }

        // Content
        public string? Subject { get; }
        public string? Body { get; }
        // Set Value melalui COnstructor
        public EmailModel(List<string> to, string subject, string? body = null, string? from = null, string? fromDisplayName = null,
          List<string> cc = null, List<string> bcc = null)
        {
            To = to;
            Cc = cc ?? new List<string>();
            Bcc = bcc ?? new List<string>();

            From = from;
            FromDisplayName = fromDisplayName;

            Subject = subject;
            Body = body;
        }


    }
}
