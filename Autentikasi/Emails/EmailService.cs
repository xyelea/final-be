﻿using otomobil.Emails.Template;
using MailKit.Net.Smtp;
using Microsoft.Extensions.Options;
using MimeKit;
using RazorEngineCore;
using System.Text;

namespace otomobil.Emails
{
    public class EmailService
    {
        private readonly EmailSettings _settings;
        public EmailService(IOptions<EmailSettings> settings)
        {
            _settings = settings.Value;
        }

        public async Task<bool> SendAsync(EmailModel model, CancellationToken ct = default)
        {
            try
            {
                var mail = new MimeMessage();

                //Sender
                mail.From.Add(new MailboxAddress(_settings.FromDisplayName, model.From ?? _settings.From));
                mail.Sender = new MailboxAddress(model.FromDisplayName ?? _settings.FromDisplayName, model.From ?? _settings.From);

                // Receiver
                foreach (string item in model.To.Where(x => !string.IsNullOrWhiteSpace(x)))
                {
                    mail.To.Add(MailboxAddress.Parse(item));
                }

                if (model.Cc != null)
                {
                    foreach (string item in model.Cc.Where(x => !string.IsNullOrWhiteSpace(x)))
                    {
                        mail.Cc.Add(MailboxAddress.Parse(item));
                    }
                }

                if (model.Bcc != null)
                {
                    foreach (string item in model.Bcc.Where(x => !string.IsNullOrWhiteSpace(x)))
                    {
                        mail.Bcc.Add(MailboxAddress.Parse(item));
                    }
                }

                //Content
                var body = new BodyBuilder();
                mail.Subject = model.Subject;
                body.HtmlBody = model.Body;
                mail.Body = body.ToMessageBody();

                //Mail Sent Process
                using var smtp = new SmtpClient();

                await smtp.ConnectAsync(_settings.Host, _settings.Port, true, ct);

                await smtp.AuthenticateAsync(_settings.UserName, _settings.Password, ct);

                await smtp.SendAsync(mail, ct);

                await smtp.DisconnectAsync(true, ct);

                return true;

            }
            catch
            {
                throw;
            }
        }

        public string GetEmailTemplate(string emailTemplate, EmailActivation activation)
        {
            string mailTemplate = LoadTemplate(emailTemplate);

            IRazorEngine razorEngine = new RazorEngine();
            IRazorEngineCompiledTemplate modifiedTemplate = razorEngine.Compile(mailTemplate);

            return modifiedTemplate.Run(activation);
        }

        private string LoadTemplate(string emailTemplate)
        {
            string baseDirectory = AppDomain.CurrentDomain.BaseDirectory;
            string templateDirectory = Path.Combine(baseDirectory, "Emails/Template");
            string templatePath = Path.Combine(templateDirectory, $"{emailTemplate}.cshtml");

            using FileStream fileStream = new FileStream(templatePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
            using StreamReader streamReader = new StreamReader(fileStream, Encoding.Default);

            string mailTemplate = streamReader.ReadToEnd();

            streamReader.Close();

            return mailTemplate;
        }


    }
}
